import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class SaveLoader {
    private File file;
    private ShoppingBag bag;

    SaveLoader(String filename) {
        this.file = new File(filename);
        this.bag = new ShoppingBag();
    }

    public void save() throws FileNotFoundException {
        PrintWriter writer = new PrintWriter(file);
        writer.println(bag.getBagNumber());
        writer.println(bag.getUsername());
        writer.print(new SimpleDateFormat("dd/MM/yyyy").format(bag.getDate()));
        for (Product product : bag.getProducts()) {
            writer.print(new StringBuilder("\n")
                    .append(product.getName())
                    .append(" ")
                    .append(product.getPrice())
                    .append(" ")
                    .append(product.getAmount()));
        }
        writer.flush();
    }

    public void load() throws FileNotFoundException {
        try {
            Scanner scanner = new Scanner(file);
            Integer bagNumber = Integer.parseInt(scanner.nextLine());
            String username = scanner.nextLine();
            String dateStr = scanner.nextLine();
            Date date = new SimpleDateFormat("dd/MM/yyyy").parse(dateStr);
            ArrayList<Product> products = new ArrayList<>();
            while (scanner.hasNextLine()) {
                Product product = new Product(scanner.next(), scanner.nextDouble(), scanner.nextInt());
                products.add(product);
            }
            bag = new ShoppingBag(bagNumber, username, date, products);
        } catch (ParseException e) {
            System.out.println("Date parsing error!");
            System.exit(0);
        }
    }

    public ShoppingBag getBag() {
        return bag;
    }
}
