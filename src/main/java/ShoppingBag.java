import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ShoppingBag {
    private Integer bagNumber;
    private String username;
    private Date date;
    private ArrayList<Product> products;

    ShoppingBag(){
        this.products = new ArrayList<>();
    }

    ShoppingBag(Integer bagNumber, String username, Date date, ArrayList<Product> products) {
        this.bagNumber = bagNumber;
        this.username = username;
        this.date = date;
        this.products = products;
    }

    public Product getProductByName(String name) throws ArrayIndexOutOfBoundsException {
        for (Product product : products) {
            if (product.getName().equals(name))
                return product;
        }
        throw new ArrayIndexOutOfBoundsException();
    }

    public Double getTotalCost() {
        Double total = 0.0;
        for (Product product : products) {
            total = total + product.getTotal();
        }
        return total;
    }

    public void refreshProduct(String name, Integer newAmount) {
        getProductByName(name).setAmount(newAmount);
    }

    public Integer getBagNumber() {
        return bagNumber;
    }

    public String getUsername() {
        return username;
    }

    public Date getDate() {
        return date;
    }

    public ArrayList<Product> getProducts() {
        return products;
    }

    public String toString() {
        StringBuilder result = new StringBuilder("Заказ №");
        DateFormat f1 = new SimpleDateFormat("dd.MM.yyyy");
        result.append(this.getBagNumber())
                .append(" ")
                .append(this.getUsername())
                .append(" ")
                .append(f1.format(this.getDate()))
                .append("\n\n");
        result.append("Название Цена Количество Сумма\n");
        for (Product product : this.getProducts()) {
            result.append(product.getName())
                    .append(" ")
                    .append(product.getPrice())
                    .append(" ")
                    .append(product.getAmount())
                    .append(" ")
                    .append(product.getTotal())
                    .append("\n");
        }
        result.append("\nИтого: ").append(this.getTotalCost());
        return result.toString();
    }
}
