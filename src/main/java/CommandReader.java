public class CommandReader {
    private ShoppingBag bag;
    CommandReader(ShoppingBag bag) {
        this.bag = bag;
    }

    public void parse(String command) {
        String[] splitCmd = command.split(" ");
        if (splitCmd.length == 2) {
            try {
                Integer newAmount = Integer.parseInt(splitCmd[1]);
                if (newAmount <= 0)
                    System.out.println("Amount can't be less or equal to zero!");
                else {
                    bag.refreshProduct(splitCmd[0], newAmount);
                    System.out.println("Product updated successfully!");
                }
            } catch (NumberFormatException e) {
                System.out.println("Unknown command!");
            } catch (ArrayIndexOutOfBoundsException e1) {
                System.out.println(String.format("Product \"%s\" not found!", splitCmd[0]));
            }
        } else if (splitCmd.length == 1) {
            if (command.equals("help")) {
                System.out.println("print - print current shopping bag;\n" +
                        "%product_name% %amount% - find product with name equal to %product_name% and set its amount to %amount%");
            } else if (command.equals("print")) {
                System.out.println(bag.toString());
            } else {
                System.out.println("Unknown command!");
            }
        } else {
            System.out.println("Unknown command!");
        }
    }
}
