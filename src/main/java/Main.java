import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        SaveLoader sl = new SaveLoader("products.txt");
        try {
            sl.load();
        } catch (FileNotFoundException e) {
            System.out.println("File not found!");
            return;
        }
        CommandReader reader = new CommandReader(sl.getBag());
        Scanner sc = new Scanner(System.in);
        reader.parse("help");
        while (sc.hasNextLine()) {
            reader.parse(sc.nextLine());
            try {
                sl.save();
            } catch (FileNotFoundException e) {
                System.out.println("File not found!");
            }
        }

    }
}
