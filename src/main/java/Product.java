public class Product {
    private String name;
    private Double price;
    private Integer amount;
    Product(String name, Double price, Integer amount) {
        this.amount = amount;
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Double getTotal(){
        return price * amount;
    }
}
